### What is this repository for? ###

* Web application with NBA games scores, statistics, players information, graphs etc.
* Version 1.0

### How do I get set up? ###

* To run app you need python 3 with all necessary packages, database with all required tables.
* PIP packages:


Package           | Version
----------------- | ---------
Flask	|1.0.2
Flask-Bootstrap	|3.3.7.1
Flask-SQLAlchemy	|2.3.2
Jinja2	|2.10
MarkupSafe	|1.0
SQLAlchemy	|1.2.7
Werkzeug	|0.14.1
certifi	|2018.4.16
chardet	|3.0.4
click	|6.7
dominate	|2.3.1
idna	|2.6
itsdangerous	|0.24
pip	|9.0.1
psycopg2	|2.7.4
pygal	|2.4.0
requests	|2.18.4
setuptools	|28.8.0
urllib3	|1.22
visitor	|0.1.3
bs4	|0.0.1

* Configuration of environment is in config/ catalog.
* You can find database table schemas in sql_tables_schemas.sql file.

### Who do I talk to? ###

* tomekk3108@gmail.com