import pygal
import random
from pygal import Config
from ..models import Team, Player, Game
from ..main import routes
from sqlalchemy import and_


def teams_avg_age():
    team_list = Team.query.all()
    for team in team_list:
        team.avg_age = calculate_avg_age(team)
        team.full_name = team.city + ' ' + team.name
    team_list.sort(key=lambda x: x.avg_age, reverse=True)

    config = Config()
    config.show_legend = False
    config.human_readable = True
    config.range = (20, 32)
    config.width = 450
    config.height = 350
    config.zero = 20

    bar_chart = pygal.HorizontalBar(config)
    bar_chart.title = 'Average age of NBA teams players.'
    bar_chart.x_labels = map(str, [i.full_name for i in team_list])
    bar_chart.add('Team', [{'value': i.avg_age, 'color': i.color} for i in team_list])
    bar_chart.render_to_file('app/static/graphs/avg_ages.svg')


def teams_avg_weight():
    team_list = Team.query.all()
    for team in team_list:
        team.avg_weight = calculate_avg_weight(team)
        team.full_name = team.city + ' ' + team.name
    team_list.sort(key=lambda x: x.avg_weight, reverse=True)

    config = Config()
    config.show_legend = False
    config.human_readable = True
    config.width = 450
    config.height = 350
    config.range = (90, 110)
    config.zero = 90

    bar_chart = pygal.HorizontalBar(config)
    bar_chart.title = 'Average weight of NBA teams players.'
    bar_chart.x_labels = map(str, [i.full_name for i in team_list])
    bar_chart.add('Team', [{'value': i.avg_weight, 'color': i.color} for i in team_list])
    bar_chart.render_to_file('app/static/graphs/avg_weights.svg')


def teams_avg_height():
    team_list = Team.query.all()
    for team in team_list:
        team.avg_height = calculate_avg_height(team)
        team.full_name = team.city + ' ' + team.name
    team_list.sort(key=lambda x: x.avg_height, reverse=True)

    config = Config()
    config.show_legend = False
    config.human_readable = True
    config.width = 450
    config.height = 350
    config.range = (190, 210)
    config.zero = 190

    bar_chart = pygal.HorizontalBar(config)
    bar_chart.title = 'Average height of NBA teams players.'
    bar_chart.x_labels = map(str, [i.full_name for i in team_list])
    bar_chart.add('Team', [{'value': i.avg_height, 'color': i.color} for i in team_list])
    bar_chart.render_to_file('app/static/graphs/avg_heights.svg')


def calculate_avg_age(team):
    team_players = Player.query.filter(Player.team_id == team.id, Player.birth_date != None).all()
    team_players_age = [routes.calculate_age(i.birth_date) for i in team_players]
    average = round(sum(team_players_age)/len(team_players), 2)
    return average


def calculate_avg_weight(team):
    team_players = Player.query.filter(and_(Player.team_id == team.id, Player.weight != None)).all()
    team_player_weight = [i.weight for i in team_players]
    average = round(sum(team_player_weight)/len(team_players), 2)
    return average


def calculate_avg_height(team):
    team_players = Player.query.filter(and_(Player.team_id == team.id, Player.height != None)).all()
    team_player_height = [i.height for i in team_players]
    average = round(sum(team_player_height)/len(team_players), 2)
    return average


def random_color():
    return str(tuple([random.randint(100, 255), random.randint(50, 255), random.randint(50, 255), 1]))
