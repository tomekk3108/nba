from flask import render_template
from . import main
from ..models import Team, Player, Game, Standings, PlayerGameStats, PlayerTotalStats
from datetime import date
from ..graphs import team_graphs
from sqlalchemy import or_, and_, desc


@main.route('/')
def index():
    return render_template('index.html')


@main.route('/players')
def players():
    players_list = Player.query.join(Team, Player.team_id == Team.id).add_columns(Team.short_name).all()
    return render_template('players.html', players=players_list, date=date, calculate_age=calculate_age)


@main.route('/player/<player_id>')
def player(player_id):
    player = Player.query.filter_by(id=player_id).first()
    player_stats = PlayerTotalStats.query.filter_by(player_id=player_id).all()
    team = Team.query.filter_by(id=player.team_id).first()
    return render_template('player.html', player=player, team=team, player_stats=player_stats)


@main.route('/teams')
def teams():
    team_list = Team.query.order_by('city asc').all()
    return render_template('teams.html', teams=team_list)


@main.route('/team/<team_id>')
def team(team_id):
    team_players = Player.query.filter(and_(Player.team_id == team_id, Player.active == True)).all()
    team = Team.query.filter_by(id=team_id).first()
    team_list = ['Zero index']
    team_list += Team.query.order_by(Team.id).all()
    games = Game.query.filter(or_(Game.team_home_id == team_id, Game.team_away_id == team_id)).order_by(
        'game_date desc').all()
    standing = Standings.query.filter_by(team_id=team_id).first()
    return render_template('team.html', team=team, team_players=team_players, date=date, calculate_age=calculate_age,
                           standing=standing, games=games, teams=team_list)


@main.route('/graphs')
def graphs():
    team_graphs.teams_avg_age()
    team_graphs.teams_avg_weight()
    team_graphs.teams_avg_height()
    return render_template('graphs.html')


@main.route('/leagueleaders')
def league_leaders():
    top_reb = PlayerTotalStats.query.join(Player, Player.id == PlayerTotalStats.player_id)\
        .filter(and_(Player.id == PlayerTotalStats.player_id, PlayerTotalStats.season_stage == 'REG')).add_columns(Player.first_name, Player.last_name, Player.id,
                                                              PlayerTotalStats.oreb_tot, PlayerTotalStats.dreb_tot,
                                                              PlayerTotalStats.games)\
        .order_by(desc(1.0*(PlayerTotalStats.dreb_tot + PlayerTotalStats.oreb_tot)/PlayerTotalStats.games)).limit(10).all()

    top_pts = PlayerTotalStats.query.join(Player, Player.id == PlayerTotalStats.player_id)\
        .filter(and_(Player.id == PlayerTotalStats.player_id, PlayerTotalStats.season_stage == 'REG')).add_columns(Player.first_name, Player.last_name, Player.id,
                                                              PlayerTotalStats.pts_tot, PlayerTotalStats.games)\
        .order_by(desc(1.0*(PlayerTotalStats.pts_tot)/PlayerTotalStats.games)).limit(10).all()
    top_ast = PlayerTotalStats.query.join(Player, Player.id == PlayerTotalStats.player_id)\
        .filter(and_(Player.id == PlayerTotalStats.player_id, PlayerTotalStats.season_stage == 'REG')).add_columns(Player.first_name, Player.last_name, Player.id,
                                                              PlayerTotalStats.ast_tot, PlayerTotalStats.games)\
        .order_by(desc(1.0*(PlayerTotalStats.ast_tot)/PlayerTotalStats.games)).limit(10).all()
    return render_template('league_leaders.html', top_pts=top_pts, top_reb=top_reb, top_ast=top_ast)


@main.route('/standings')
def standings():
    standings_w = Standings.query.join(Team, Standings.team_id == Team.id).filter(Team.conference == 'W')\
        .add_columns(Team.city, Team.name).order_by('standings.win_perc desc').all()
    standings_e = Standings.query.join(Team, Standings.team_id == Team.id).filter(Team.conference == 'E')\
        .add_columns(Team.city, Team.name).order_by('standings.win_perc desc').all()
    return render_template('standings.html', standings_w=standings_w, standings_e=standings_e)


@main.route('/games')
def games():
    teams = ['Zero index']
    teams += Team.query.order_by(Team.id).all()
    games = Game.query.order_by(Game.game_date).all()
    return render_template('games.html', games=games, teams=teams)


@main.route('/game/<game_id>')
def game(game_id):
    game = Game.query.filter_by(id=game_id).first()
    game_stats_home = PlayerGameStats.query.join(Player, PlayerGameStats.player_id == Player.id)\
        .filter(PlayerGameStats.game_id == game.id, PlayerGameStats.team_id == game.team_home_id, PlayerGameStats.min +
                PlayerGameStats.sec > 0)\
        .add_columns(Player.first_name, Player.last_name).order_by('min desc').all()
    game_stats_away = PlayerGameStats.query.join(Player, PlayerGameStats.player_id == Player.id)\
        .filter(PlayerGameStats.game_id == game.id, PlayerGameStats.team_id == game.team_away_id, PlayerGameStats.min +
                PlayerGameStats.sec > 0)\
        .add_columns(Player.first_name, Player.last_name).order_by('min desc').all()
    game_players = Player.query.filter(or_(Player.team_id == game.team_home_id, Player.team_id == game.team_away_id)).all()
    return render_template('game.html', game=game, game_stats_home=game_stats_home, game_stats_away=game_stats_away,
                           players=game_players)


def calculate_age(born):
    today = date.today()
    return today.year - born.year - ((today.month, today.day) < (born.month, born.day))
