from . import db
from sqlalchemy import and_


class Player(db.Model):
    __tablename__ = 'players'
    id = db.Column(db.Integer, primary_key=True)
    nba_id = db.Column(db.Integer, unique=True)
    first_name = db.Column(db.String)
    last_name = db.Column(db.String)
    position = db.Column(db.String)
    birth_date = db.Column(db.Date)
    birth_country = db.Column(db.String)
    birth_city = db.Column(db.String)
    height = db.Column(db.Integer)
    weight = db.Column(db.Float)
    team_id = db.Column(db.Integer)
    jersey_number = db.Column(db.String)
    active = db.Column(db.Boolean, default=False)

    @staticmethod
    def add_player(player):
        db.session.add(player)
        db.session.commit()

    @staticmethod
    def add_or_update(player):
        if Player.query.filter_by(nba_id=player.nba_id).first() is not None:
            print(f'Player {player} already in database')
            old_player = Player.query.filter_by(nba_id=player.nba_id).first()
            old_player.first_name = player.first_name
            old_player.last_name = player.last_name
            old_player.position = player.position
            old_player.birth_city = player.birth_city
            old_player.birth_country = player.birth_country
            old_player.birth_date = player.birth_date
            old_player.height = player.height
            old_player.weight = player.weight
            old_player.team_id = player.team_id
            old_player.jersey_number = player.jersey_number
            old_player.active = player.active

            db.session.commit()
            print(f'Player {old_player} updated.')
        else:
            db.session.add(player)
            db.session.commit()
            print(f'Player {player} added to database.')

    def __repr__(self):
        return f'<Player {self.first_name} {self.last_name}>'


class Team(db.Model):
    __tablename__ = 'teams'
    id = db.Column(db.Integer, primary_key=True)
    nba_id = db.Column(db.Integer, unique=True)
    city = db.Column(db.String, unique=True)
    name = db.Column(db.String, unique=True)
    short_name = db.Column(db.String, unique=True)
    color = db.Column(db.String)
    conference = db.Column(db.String)
    division = db.Column(db.String)

    @staticmethod
    def add_team(team):
        db.session.add(team)
        db.session.commit()

    def __repr__(self):
        return f'<Team {self.city} {self.name}>'


class Game(db.Model):
    __tablename__ = 'games'
    id = db.Column(db.Integer, primary_key=True)
    nba_id = db.Column(db.BigInteger, unique=True)
    team_home_id = db.Column(db.Integer)
    team_away_id = db.Column(db.Integer)
    points_home = db.Column(db.Integer)
    points_away = db.Column(db.Integer)
    season_stage = db.Column(db.String)
    game_date = db.Column(db.Date)
    processed_standings = db.Column(db.Boolean, default=False)
    processed_details = db.Column(db.Boolean, default=False)

    @staticmethod
    def add_game(game):
        db.session.add(game)
        db.session.commit()

    def set_processed_standings(self):
        self.processed_standings = True
        db.session.commit()
        print(f'Game: {self} stanadings processed.')

    def set_processed_details(self):
        self.processed_details = True
        db.session.commit()
        print(f'Game: {self} details processed.')

    @staticmethod
    def add_or_update(game, counter):
        if Game.query.filter_by(nba_id=game.nba_id).first() is not None:
            old_game = Game.query.filter_by(nba_id=game.nba_id).first()
            print(f'Game {old_game.id} already in database.')
            old_game.team_home_id = game.team_home_id
            old_game.team_away_id = game.team_away_id
            old_game.points_home = game.points_home
            old_game.points_away = game.points_away
            old_game.season_stage = game.season_stage
            old_game.game_date = game.game_date
            db.session.commit()
            print(f'Game {old_game.id} updated.')
            counter['updated'] += 1
            return counter
        else:
            db.session.add(game)
            db.session.commit()
            print(f'Game {game.id} added to database.')
            counter['added'] += 1
            return counter

    def __repr__(self):
        return f'<Game id: {self.id}>'


class PlayerGameStats(db.Model):
    __tablename__ = 'player_game_stats'
    id = db.Column(db.Integer, primary_key=True)
    game_id = db.Column(db.BigInteger)
    season = db.Column(db.String)
    season_stage = db.Column(db.String)
    player_id = db.Column(db.Integer)
    team_id = db.Column(db.Integer)
    min = db.Column(db.SmallInteger)
    sec = db.Column(db.SmallInteger)
    pts = db.Column(db.SmallInteger)
    oreb = db.Column(db.SmallInteger)
    dreb = db.Column(db.SmallInteger)
    ast = db.Column(db.SmallInteger)
    stl = db.Column(db.SmallInteger)
    tov = db.Column(db.SmallInteger)
    blk = db.Column(db.SmallInteger)
    pf = db.Column(db.SmallInteger)
    fgm = db.Column(db.SmallInteger)
    fga = db.Column(db.SmallInteger)
    ftm = db.Column(db.SmallInteger)
    fta = db.Column(db.SmallInteger)
    tpm = db.Column(db.SmallInteger)
    tpa = db.Column(db.SmallInteger)
    pm = db.Column(db.SmallInteger)
    processed_total = db.Column(db.Boolean, default=False)

    @staticmethod
    def add_game_stat(player_game_stat):
        db.session.add(player_game_stat)
        db.session.commit()

    def set_processed_total(self):
        self.processed_total = True
        db.session.commit()
        print(f'Total stats for line {self} processed.')


class PlayerTotalStats(db.Model):
    __tablename__ = 'player_total_stats'
    id = db.Column(db.Integer, primary_key=True)
    player_id = db.Column(db.Integer)
    team_id = db.Column(db.Integer)
    season_stage = db.Column(db.String)
    season = db.Column(db.String)
    games = db.Column(db.SmallInteger, default=0)
    min_tot = db.Column(db.SmallInteger, default=0)
    sec_tot = db.Column(db.SmallInteger, default=0)
    pts_tot = db.Column(db.SmallInteger, default=0)
    oreb_tot = db.Column(db.SmallInteger, default=0)
    dreb_tot = db.Column(db.SmallInteger, default=0)
    ast_tot = db.Column(db.SmallInteger, default=0)
    stl_tot = db.Column(db.SmallInteger, default=0)
    tov_tot = db.Column(db.SmallInteger, default=0)
    blk_tot = db.Column(db.SmallInteger, default=0)
    pf_tot = db.Column(db.SmallInteger, default=0)
    fgm_tot = db.Column(db.SmallInteger, default=0)
    fga_tot = db.Column(db.SmallInteger, default=0)
    ftm_tot = db.Column(db.SmallInteger, default=0)
    fta_tot = db.Column(db.SmallInteger, default=0)
    tpm_tot = db.Column(db.SmallInteger, default=0)
    tpa_tot = db.Column(db.SmallInteger, default=0)
    pm_tot = db.Column(db.SmallInteger, default=0)

    @staticmethod
    def add_player_line(player_total_stats):
        db.session.add(player_total_stats)
        db.session.commit()

    @staticmethod
    def update(pgs):
        pts = PlayerTotalStats.query.filter(and_(PlayerTotalStats.player_id == pgs.player_id, PlayerTotalStats.season_stage == pgs.season_stage)).first()
        pts.games += 1
        pts.min_tot += pgs.min
        if (pts.sec_tot + pgs.sec) > 60:
            pts.min_tot += 1
            pts.sec_tot = ((pts.sec_tot + pgs.sec) % 60)
        else:
            pts.sec_tot += pgs.sec
        pts.pts_tot += pgs.pts
        pts.oreb_tot += pgs.oreb
        pts.dreb_tot += pgs.dreb
        pts.ast_tot += pgs.ast
        pts.stl_tot += pgs.stl
        pts.tov_tot += pgs.tov
        pts.blk_tot += pgs.blk
        pts.pf_tot += pgs.pf
        pts.fgm_tot += pgs.fgm
        pts.fga_tot += pgs.fga
        pts.ftm_tot += pgs.ftm
        pts.fta_tot += pgs.fta
        pts.tpm_tot += pgs.tpm
        pts.tpa_tot += pgs.tpa
        pts.pm_tot += pgs.pm

        db.session.commit()


class Standings(db.Model):
    __tablename__ = 'standings'
    id = db.Column(db.Integer, primary_key=True)
    team_id = db.Column(db.Integer, unique=True, nullable=False)
    wins = db.Column(db.Integer, default=0)
    losses = db.Column(db.Integer, default=0)
    conf_wins = db.Column(db.String, default=0)
    conf_losses = db.Column(db.String, default=0)
    div_wins = db.Column(db.String, default=0)
    div_losses = db.Column(db.String, default=0)
    home_wins = db.Column(db.String, default=0)
    home_losses = db.Column(db.String, default=0)
    away_wins = db.Column(db.String, default=0)
    away_losses = db.Column(db.String, default=0)
    win_perc = db.Column(db.Float, default=0)
    games_behind = db.Column(db.Float)
    last_ten = db.Column(db.String)
    streak = db.Column(db.String)

    @staticmethod
    def add_team(standing):
        db.session.add(standing)
        db.session.commit()

    @staticmethod
    def update(standing):
        old_standing = Standings.query.filter_by(id=standing.id)
        old_standing = standing
        db.session.commit()

    def add_streak_win(self):
        if self.streak is None or self.streak[0] == 'L':
            self.streak = 'W1'
        else:
            current_int_streak = int(self.streak[1:])
            self.streak = 'W' + str(current_int_streak + 1)

    def add_streak_loss(self):
        if self.streak is None or self.streak[0] == 'W':
            self.streak = 'L1'
        else:
            current_int_streak = int(self.streak[1:])
            self.streak = 'L' + str(current_int_streak + 1)

    def __init__(self, team_id):
        self.team_id = team_id
