import os
import psycopg2

username = 'postgres'
password = 'haslo123'
host = 'localhost:5432'
db_name = 'postgres'
DEBUG = True
SECRET_KEY = 'top secret!'
SQLALCHEMY_DATABASE_URI = f'postgresql+psycopg2://{username}:{password}@{host}/{db_name}'
