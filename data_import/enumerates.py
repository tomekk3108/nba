from enum import Enum


class SeasonStage(Enum):
    PRE = 1
    REG = 2
    PLA = 4
