import time
import json
import requests
from app.models import Game, PlayerGameStats, Player, PlayerTotalStats, Team
from sqlalchemy import and_


def import_game_details():
    print('Starting downloading game stats.')
    start_time = time.time()
    unprocessed_games = Game.query.filter_by(processed_details=False).all()
    for game in unprocessed_games:
        game_nba_id = '00' + str(game.nba_id)
        url = f'http://data.nba.com/data/10s/v2015/json/mobile_teams/nba/2017/scores/gamedetail/{game_nba_id}' \
              f'_gamedetail.json'
        print(url)
        r = requests.get(url)
        try:
            game_details_json = r.json()
        except json.decoder.JSONDecodeError:
            print(f'Problem with json file for game {game_nba_id}.'
                  f'Url: {url}')
            continue
        player_game_list = game_details_json['g']['hls']['pstsg'] + game_details_json['g']['vls']['pstsg']
        for player_game in player_game_list:
            if player_game['totsec'] == 0:
                continue
            game_id = game.id
            player_nba_id = player_game['pid']
            player = Player.query.filter_by(nba_id=player_nba_id).first()
            if player is not None:
                if pgs_exists(player.id, game_id):
                    print(f'Game stats of {player} in game {game} already in database.')
                    continue
                else:
                    team_id = player.team_id
                    player_id = player.id
                    season = '17/18'
                    season_stage = game.season_stage
                    min = player_game['min']
                    sec = player_game['sec']
                    pts = player_game['pts']
                    oreb = player_game['oreb']
                    dreb = player_game['dreb']
                    ast = player_game['ast']
                    stl = player_game['stl']
                    tov = player_game['tov']
                    blk = player_game['blk']
                    pf = player_game['pf']
                    fgm = player_game['fgm']
                    fga = player_game['fga']
                    ftm = player_game['ftm']
                    fta = player_game['fta']
                    tpm = player_game['tpm']
                    tpa = player_game['tpa']
                    pm = player_game['pm']
                    player_game_stat = PlayerGameStats(game_id=game_id, player_id=player_id, season=season,
                                                       season_stage=season_stage, team_id=team_id, min=min, sec=sec,
                                                       pts=pts, oreb=oreb, dreb=dreb, ast=ast, stl=stl, tov=tov, blk=blk,
                                                       pf=pf, fgm=fgm, fga=fga, ftm=ftm, fta=fta, tpm=tpm, tpa=tpa, pm=pm)
                    PlayerGameStats.add_game_stat(player_game_stat)
                    print(f'Players {player_id} detail stats from game {game_id} added to database.')
            else:
                player_game_stat = PlayerGameStats(game_id=game_id)
                PlayerGameStats.add_game_stat(player_game_stat)
                print(f'Player with nba_id: {player_nba_id} not found in database. Added empty result to reconsiliate.')

        game.set_processed_details()
        print(f'Game id {game.id} detail stats processed.')

    elapsed_time = time.time() - start_time
    print(f'Elapsed time of downloading and saving all players data: {elapsed_time}')


def calculate_total_stats():
    player_game_stats = PlayerGameStats.query.filter_by(processed_total=False).all()
    for line in player_game_stats:
        if line.player_id is None:
            print(f'Could not find player for game: {line.game_id}')
            continue
        player_id = line.player_id
        if PlayerTotalStats.query.filter(and_(PlayerTotalStats.player_id == player_id, PlayerTotalStats.season_stage == line.season_stage)).first() is None:
            pts = PlayerTotalStats(player_id=player_id, team_id=line.team_id, season_stage=line.season_stage,
                                   season=line.season)
            PlayerTotalStats.add_player_line(pts)
            print(f'Player {player_id} total stats record added to db.')
        PlayerTotalStats.update(line)
        line.set_processed_total()
        print(f'Total stats for player {player_id} updated.')


def pgs_exists(player_id, game_id):
    if PlayerGameStats.query.filter(and_(PlayerGameStats.player_id == player_id, PlayerGameStats.game_id == game_id)).first() is not None:
        return True
    else:
        return False


def test():
    game_nba_id = '00' + '21700024'
    url = f'http://data.nba.com/data/10s/v2015/json/mobile_teams/nba/2017/scores/gamedetail/{game_nba_id}' \
          f'_gamedetail.json'
    r = requests.get(url)
    game_details_json = r.json()
    player_game_list = game_details_json['g']['hls']['pstsg']
    print(game_details_json['g'])
