import requests
from app.models import Game, Team
from datetime import date, timedelta
from .enumerates import SeasonStage


def daily_games_import(date_from=date.today() - timedelta(days=1)):
    date_to = date.today()
    delta = date_to - date_from
    counter = {'added': 0, 'updated': 0}
    for i in range(delta.days):
        searching_date = date_from + timedelta(days=i)
        year = searching_date.year
        month = searching_date.month
        day = searching_date.day
        str_day = add_zero(day)
        str_month = add_zero(month)
        searching_date_string = str(year) + str_month + str_day
        url = f'https://data.nba.net/prod/v2/{searching_date_string}/scoreboard.json'

        response = requests.get(url, verify=False)                              # try to add verify on other device
        scoreboard = response.json()
        games = scoreboard['games']

        for game in games:
            nba_id = game['gameId']
            try:
                team_home_id = Team.query.filter_by(nba_id=game['hTeam']['teamId']).first().id
                team_away_id = Team.query.filter_by(nba_id=game['vTeam']['teamId']).first().id
            except AttributeError:
                print(f'Did not found teams for game id {nba_id}')
                continue
            points_home = game['hTeam']['score']
            points_away = game['vTeam']['score']
            season_stage = SeasonStage(int(game['seasonStageId'])).name
            game_date = searching_date

            game = Game(nba_id=nba_id,
                        team_home_id=team_home_id,
                        team_away_id=team_away_id,
                        points_home=points_home,
                        points_away=points_away,
                        season_stage=season_stage,
                        game_date=game_date)

            counter = Game.add_or_update(game, counter)
    print(f'Updated: {counter["updated"]} games.'
          f'Added: {counter["added"]} games.')


def add_zero(num):
    if num < 10:
        return '0' + str(num)
    else:
        return str(num)
