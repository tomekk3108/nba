# link for players info nba: http://www.nba.com/players/active_players.json
# link for player site with details (not sure if needed) http://www.nba.com/players/FIRST_NAME/LAST_NAME/1626156
# link for player image https://ak-static.cms.nba.com/wp-content/uploads/headshots/nba/latest/260x190/PLAYER_ID.png

import requests, urllib, time, datetime, os
from app.models import Player, Team
from bs4 import BeautifulSoup

# Constants
FOOT_CM_RATIO = 30.48
INCH_CM_RATIO = 2.54
POUND_KG_RATIO = 0.45359237


def import_players():
    print('Starting inserting all nba players to database...')
    start_time = time.time()
    # url = 'http://www.nba.com/players/active_players.json'
    url = 'http://data.nba.net/10s//prod/v1/2017/players.json'
    r = requests.get(url)
    players_json = r.json()['league']['standard']
    for player in players_json:
        create_player(player)
    elapsed_time = time.time() - start_time
    print(f'Elapsed time of downloading and saving all players data: {elapsed_time}')


def create_player(player_dict):

    try:
        height_cm = int(float(player_dict['heightMeters']) * 100)
        weight_kg = float(player_dict['weightKilograms'])
    except ValueError:
        print(f'Could not read players height or weight.')
        height_cm = None
        weight_kg = None

    birth_date = string_date_to_datetime(player_dict['dateOfBirthUTC'])
    is_active = player_dict['isActive']

    try:
        team_id = Team.query.filter_by(nba_id=int(player_dict['teamId'])).first().id
    except AttributeError:
        team_id = None
    except ValueError:
        team_id = None

    player = Player(first_name=player_dict['firstName'],
                    nba_id=int(player_dict['personId']),
                    last_name=player_dict['lastName'],
                    position=player_dict['pos'],
                    jersey_number=player_dict['jersey'],
                    height=height_cm,
                    weight=weight_kg,
                    team_id=team_id,
                    birth_date=birth_date,
                    birth_country=player_dict['country'],
                    active=is_active
                    )

    if Player.query.filter_by(nba_id=int(player_dict['personId'])).first() is not None:
        Player.add_or_update(player)
    else:
        Player.add_player(player)


def string_date_to_datetime(string_date):
    date_list = string_date.split('-')
    try:
        return datetime.date(int(date_list[0]), int(date_list[1].lstrip('0')), int(date_list[2].lstrip('0')))
    except ValueError:
        print(f'Could not find birth date')
        return None


def download_players_image():
    players_list = Player.query.all()
    start_time = time.time()
    for player in players_list:
        player_nba_id = player.nba_id
        player_id = player.id
        file_path = f'app/static/players_images/{player_id}.png'
        # if not os.path.isfile(file_path):
        print(f'Trying to download image for: {player}, NBA ID: {player_nba_id}, ID: {player_id}')
        try:
            resource = urllib.request.urlopen(f'https://ak-static.cms.nba.com/wp-content/uploads/headshots/nba/latest/260x190/{player_nba_id}.png')
            output = open(file_path, 'wb')
            output.write(resource.read())
            output.close()
        except urllib.error.HTTPError:
            print(f'Photo of {player} not available.')

        # else:
        #     print(f'Image of {player} already exists. checking for updates...')
    elapsed_time = time.time() - start_time
    print(f'Elapsed time of downloading and saving all players images: {elapsed_time}')


def imperial_to_cm(feet, inches):
    total_cm = round(feet * FOOT_CM_RATIO + inches * INCH_CM_RATIO)
    return total_cm


def imperial_to_kg(pounds):
    total_kg = round(pounds * POUND_KG_RATIO, 1)
    return total_kg


def import_birth_date(player):
    player_last_name = player.last_name.replace(' ', '_')
    player_first_name = player.first_name.replace(' ', '_')
    url = f'http://www.nba.com/players/{player_first_name}/{player_last_name}/{player.nba_id}'
    print(f'Searching for: {player} birth date.')
    try:
        page = urllib.request.urlopen(url)
        soup = BeautifulSoup(page)
        birth_date_list = soup.find_all('span', class_='nba-player-vitals__bottom-info')[0].text.strip().split('/')
        birth_date_list = list(map(int, birth_date_list))
        birth_date = datetime.date(year=birth_date_list[2], month=birth_date_list[0], day=birth_date_list[1])
        print(f'FOUND BIRTH_DATE OF {player}: {birth_date}')
        return birth_date
    except urllib.error.HTTPError as e:
        print(f'Error: {e}')
        return None
