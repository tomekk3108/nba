CREATE TABLE players(
 id SERIAL PRIMARY KEY,
 nba_id INTEGER UNIQUE NOT NULL,
 first_name VARCHAR(50) NOT NULL,
 last_name VARCHAR(50) NOT NULL,
 position VARCHAR(10) NOT NULL,
 birth_date DATE,
 birth_country VARCHAR(50),
 birth_city VARCHAR(50),
 height INTEGER,
 weight FLOAT,
 team_id INTEGER,
 jersey_number VARCHAR(2),
 active BOOLEAN NOT NULL DEFAULT FALSE
);

CREATE TABLE teams(
 id SERIAL PRIMARY KEY,
 nba_id BIGINT UNIQUE NOT NULL,
 city VARCHAR(50) NOT NULL,
 name VARCHAR(50) NOT NULL,
 short_name VARCHAR(3) NOT NULL,
 conference VARCHAR(1) NOT NULL,
 division VARCHAR(2) NOT NULL,
 color VARCHAR(7)
);

CREATE TABLE games(
 id SERIAL PRIMARY KEY,
 nba_id BIGINT UNIQUE NOT NULL,
 team_home_id INTEGER NOT NULL,
 team_away_id INTEGER NOT NULL,
 points_home INTEGER NOT NULL,
 points_away INTEGER NOT NULL,
 season_stage VARCHAR(3) NOT NULL,
 game_date DATE,
 processed_standings BOOLEAN NOT NULL DEFAULT FALSE,
 processed_details BOOLEAN NOT NULL DEFAULT FALSE
);

CREATE TABLE standings(
 id SERIAL PRIMARY KEY,
 team_id INTEGER UNIQUE NOT NULL,
 wins INTEGER,
 losses INTEGER,
 conf_wins INTEGER,
 conf_losses INTEGER,
 div_wins INTEGER,
 div_losses INTEGER,
 home_wins INTEGER,
 home_losses INTEGER,
 away_wins INTEGER,
 away_losses INTEGER,
 win_perc FLOAT,
 games_behind FLOAT,
 last_ten VARCHAR(5),
 streak VARCHAR(3)
 )

CREATE TABLE player_game_stats(
id SERIAL PRIMARY KEY,
game_id BIGINT,
player_id INTEGER,
team_id INTEGER
min SMALLINT,
sec SMALLINT,
pts SMALLINT,
oreb SMALLINT,
dreb SMALLINT,
ast SMALLINT,
stl SMALLINT,
tov SMALLINT,
blk SMALLINT,
pf SMALLINT,
fgm SMALLINT,
fga SMALLINT,
ftm SMALLINT,
fta SMALLINT,
tpm SMALLINT,
tpa SMALLINT,
pm SMALLINT,
processed_total BOOLEAN NOT NULL DEFAULT FALSE
)


CREATE TABLE player_total_stats(
id SERIAL PRIMARY KEY,
player_id INTEGER NOT NULL,
team_id INTEGER NOT NULL,
season_stage = VARCHAR(3) NOT NULL,
season VARCHAR(5),
games SMALLINT,
min_tot SMALLINT,
sec_tot SMALLINT,
pts_tot SMALLINT,
oreb_tot SMALLINT,
dreb_tot SMALLINT,
ast_tot SMALLINT,
stl_tot SMALLINT,
tov_tot SMALLINT,
blk_tot SMALLINT,
pf_tot SMALLINT,
fgm_tot SMALLINT,
fga_tot SMALLINT,
ftm_tot SMALLINT,
fta_tot SMALLINT,
tpm_tot SMALLINT,
tpa_tot SMALLINT,
pm_tot SMALLINT
)

