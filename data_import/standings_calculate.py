import time
from app.models import Team, Standings, Game
from sqlalchemy import or_, and_


# CREATE TEAM STANDING RECORD FOR ALL TEAMS
def import_teams_standings():
    print('Start inserting all nba teams to database...')
    start_time = time.time()
    teams_list = Team.query.all()
    for team in teams_list:
        team_standing = Standings(team_id=team.id)
        Standings.add_team(team_standing)


def update_standings():
    games = Game.query.filter(and_(Game.processed_standings == False, Game.season_stage == 'REG')).order_by('game_date asc').all()
    for game in games:
        if game.points_home > game.points_away:
            winner_team = Team.query.filter_by(id=game.team_home_id).first()
            looser_team = Team.query.filter_by(id=game.team_away_id).first()
            winner = Standings.query.filter_by(team_id=game.team_home_id).first()
            looser = Standings.query.filter_by(team_id=game.team_away_id).first()
            winner.home_wins += 1
            looser.away_losses += 1

        else:
            winner_team = Team.query.filter_by(id=game.team_away_id).first()
            looser_team = Team.query.filter_by(id=game.team_home_id).first()
            winner = Standings.query.filter_by(team_id=game.team_away_id).first()
            looser = Standings.query.filter_by(team_id=game.team_home_id).first()
            winner.away_wins += 1
            looser.home_losses += 1

        if winner_team.conference == looser_team.conference:
            winner.conf_wins += 1
            looser.conf_losses += 1

        if winner_team.division == looser_team.division:
            winner.div_wins += 1
            looser.div_losses += 1

        winner.wins += 1
        looser.losses += 1

        winner.add_streak_win()
        looser.add_streak_loss()

        Standings.update(winner)
        Standings.update(looser)
        game.set_processed_standings()


def calculate_win_perc(standing):
    win_perc = round(standing.wins/(standing.wins + standing.losses), 3)
    return win_perc


def update_last_ten_perc():
    team_list = Team.query.all()
    for team in team_list:
        standing = Standings.query.filter_by(team_id=team.id).first()
        last_ten = calculate_last_ten(team)
        win_perc = calculate_win_perc(standing)
        standing.last_ten = last_ten
        standing.win_perc = win_perc
        Standings.update(standing)


def calculate_last_ten(team):
    games = Game.query.filter(and_(Game.season_stage == 'REG', or_(Game.team_home_id == team.id, Game.team_away_id == team.id))).order_by('game_date desc')\
        .limit(10).all()
    wins = 0
    losses = 0
    for game in games:
        if team.id == game.team_home_id:
            if game.points_home > game.points_away:
                wins += 1
            else:
                losses += 1
        else:
            if game.points_away > game.points_home:
                wins += 1
            else:
                losses += 1

    return str(wins) + '-' + str(losses)
