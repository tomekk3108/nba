# link to team logo nba http://i.cdn.turner.com/nba/nba/assets/logos/teams/secondary/web/TEAM_SHORT_NAME.svg
import requests, time, os, urllib
from app.models import Team


def import_teams():
    print('Start inserting all nba teams to database...')
    start_time = time.time()
    url = 'http://data.nba.com/'
    r = requests.get(url)
    teams_list = r.json()['sports_content']['teams']['team']
    for team in teams_list:
        create_team(team)
    elapsed_time = time.time() - start_time
    print(f'Elapsed time of downloading and saving all teams data: {elapsed_time}')


def import_team_logos():
    teams_list = Team.query.all()
    start_time = time.time()
    for team in teams_list:
        team_short_name = team.short_name
        team_id= team.id
        file_path = f'app/static/team_logos/{team_id}.svg'
        if not os.path.isfile(file_path):
            print(f'Trying to download image for: {team}, NBA ID: {team.nba_id}, ID: {team.id}')
            try:
                resource = urllib.request.urlopen(
                    f'http://i.cdn.turner.com/nba/nba/assets/logos/teams/primary/web/{team_short_name}.svg')
                output = open(file_path, 'wb')
                output.write(resource.read())
                output.close()
            except urllib.error.HTTPError:
                print(f'Logo of {team} not available.')

        else:
            print(f'Image of {team} already exists.')
    elapsed_time = time.time() - start_time
    print(f'Elapsed time of downloading and saving all team logos images: {elapsed_time}')


def create_team(team):
    is_nba_team = team['is_nba_team']
    exists_in_db = Team.query.filter_by(nba_id=int(team['team_id'])).first() is not None
    if not is_nba_team or exists_in_db:
        print(team['team_name'] + team['team_nickname'] + ' already in database or is not NBA team.')
        return
    team_city = team['team_name']
    team_name = team['team_nickname']
    team_nba_id = int(team['team_id'])
    team_short_name = team['team_abbrev']

    team = Team(nba_id=team_nba_id,
                city=team_city,
                name=team_name,
                short_name=team_short_name)

    Team.add_team(team)
    print(f'Team {team} added to database.')
