#!/usr/bin/env python
import os
from app import create_app, db
from data_import import players_import, teams_import, games_import, standings_calculate, game_details
from datetime import date

if __name__ == '__main__':
    config_name = os.environ.get('FLASK_CONFIG') or 'development'
    print(f' * Loading configuration "{config_name}"')
    app = create_app(config_name)
    with app.app_context():
        db.create_all()
        # players_import.import_players()
        # players_import.download_players_image()
        # teams_import.import_teams()
        # teams_import.import_team_logos()
        # standings_calculate.import_teams_standings()
        # games_import.daily_games_import(date_from=date(2018, 4, 10))
        # standings_calculate.update_standings()
        # standings_calculate.update_last_ten_perc()
        # game_details.import_game_details()
        # game_details.calculate_total_stats()
    app.run()
